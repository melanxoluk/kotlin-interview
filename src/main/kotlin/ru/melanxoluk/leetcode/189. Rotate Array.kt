package ru.melanxoluk.leetcode

class Solution189 {
    fun rotate(nums: IntArray, k: Int): Unit {
        (nums.drop(nums.size - k) + nums.take(nums.size - k))
            .forEachIndexed { i, num -> nums[i] = num }
    }
}
