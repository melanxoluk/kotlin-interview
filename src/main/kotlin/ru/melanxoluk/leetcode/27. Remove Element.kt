package ru.melanxoluk.leetcode

class Solution27 {
    fun removeElement(nums: IntArray, `val`: Int): Int {
        val filtered = nums.filter { it != `val` }
        for (i in 1 until filtered.size) {
            nums[i] = filtered[i]
        }
        
        return filtered.size
    }
}
