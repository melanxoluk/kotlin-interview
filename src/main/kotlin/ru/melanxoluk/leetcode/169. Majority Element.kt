package ru.melanxoluk.leetcode

class Solution169 {
    fun majorityElement(nums: IntArray): Int {
        val counters = mutableMapOf<Int, Int>()
        
        nums.forEach { num ->
            counters.compute(num) { _, v -> v?.plus(1) ?: 1 }
        }
        
        return counters.maxBy { it.value }.key
    }
}

fun main() {
    Solution169().majorityElement(intArrayOf(3, 2, 3))
}
