package ru.melanxoluk.leetcode

class Solution135 {
    fun candy(ratings: IntArray): Int {
        val candies = ratings.mapTo(mutableListOf()) { 1 }
        
        for (i in 1 until ratings.size) {
            if (ratings[i] > ratings[i - 1])
                candies[i] = candies[i - 1] + 1
        }
        
        for (i in ratings.size - 2 downTo 0) {
            if (ratings[i] > ratings[i + 1])
                if (candies[i] <= candies[i + 1])
                    candies[i] = candies[i + 1] + 1
        }
        
        return candies.sum()
    }
}

fun main() {
    println(
        Solution135().candy(intArrayOf(1, 2, 3 ,2, 1))
    )
}
