package ru.melanxoluk.leetcode

class Solution88 {
    fun merge(nums1: IntArray, m: Int, nums2: IntArray, n: Int): Unit {
        val merge = mutableListOf<Int>()

        var nums1Index = 0
        var nums2Index = 0

        // todo: check ranges
        while (nums1Index < m && nums2Index < n) {
            val n1 = nums1[nums1Index]
            val n2 = nums2[nums2Index]
            if (n1 < n2) {
                merge.add(n1)
                nums1Index++
            } else {
                merge.add(n2)
                nums2Index++
            }
        }

        // fill with not processed array
        if (nums1Index < m) {
            for (i in nums1Index until m) {
                merge.add(nums1[i])
            }
        }

        if (nums2Index < n) {
            for (i in nums2Index until n) {
                merge.add(nums2[i])
            }
        }

        // fill target arr from temporary holder
        for (i in 0 until m + n) {
            nums1[i] = merge[i]
        }
    }
}
