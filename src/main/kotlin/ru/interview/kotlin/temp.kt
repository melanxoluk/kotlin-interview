package ru.interview.kotlin

import java.math.BigDecimal
import java.math.MathContext

//val one = BigDecimal(1, MathContext(4))
val one = 1.0
fun Double.decimal() = BigDecimal(this, MathContext(4))

fun main() {
    var sum = 100_000.0

    val commission = 0.0045
    val stopLoss = 0.01
    val takeProfit = 0.015
    val successDeals = 0.6
    
    repeat(220) {
        val availableSum = sum * (one / (one + stopLoss))
        val foundation = sum - availableSum

        val dayProfit = availableSum * takeProfit * successDeals
        val dayLost = availableSum * stopLoss * (one - successDeals)
        val commissionAmount = availableSum * commission
        sum = foundation + availableSum + dayProfit - dayLost - commissionAmount
    }

    println(sum)
}
