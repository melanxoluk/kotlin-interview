package ru.interview.kotlin.dao

import org.jetbrains.exposed.sql.batchInsert
import org.jetbrains.exposed.sql.selectAll
import ru.interview.kotlin.Person
import ru.interview.kotlin.PersonTable
import ru.interview.kotlin.Product
import ru.interview.kotlin.ProductTable
import ru.interview.kotlin.Sale
import ru.interview.kotlin.SaleTable


fun createProducts(products: List<Product>) {
    ProductTable.batchInsert(products) { product ->
        this[ProductTable.id] = product.id
        this[ProductTable.name] = product.name
        this[ProductTable.price] = product.price
    }
}

fun getProducts(): List<Product> {
    return ProductTable.selectAll().map(::Product)
}


fun createPersons(persons: List<Person>) {
    PersonTable.batchInsert(persons) { person ->
        this[PersonTable.id] = person.id
        this[PersonTable.email] = person.email
        this[PersonTable.name] = person.name
    }
}

fun getPersons(): List<Person> {
    return PersonTable.selectAll().map(::Person)
}


fun createSales(sales: List<Sale>) {
    SaleTable.batchInsert(sales) { sale ->
        this[SaleTable.id] = sale.id
        this[SaleTable.personId] = sale.personId
        this[SaleTable.productId] = sale.productId
        this[SaleTable.amount] = sale.amount
    }
}

fun getSales(): List<Sale> {
    return SaleTable.selectAll().map(::Sale)
}