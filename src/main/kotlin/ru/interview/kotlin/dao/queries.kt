package ru.interview.kotlin.dao

import ru.interview.kotlin.Person
import ru.interview.kotlin.Product
import ru.interview.kotlin.Sale


/**
 * Возвращает все покупки покупателя по его имени
 */
fun getSales(name: String): List<Sale> {
    TODO()
}

/**
 * Возвращает покупателя, который совершил самое большее количество покупок
 */
fun getMostSalesPerson(): Person {
    TODO()
}

/**
 * Возвращает все продукты для которых были найдены дубликаты
 */
fun getDuplicates(): List<Pair<Product, Product>> {
    TODO()
}