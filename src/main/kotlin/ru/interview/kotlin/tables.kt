package ru.interview.kotlin

import org.jetbrains.exposed.dao.id.IntIdTable

object PersonTable : IntIdTable("person") {
    val email = text("email")
    val name = text("name")
}

object ProductTable : IntIdTable("product") {
    val name = text("name")
    val price = decimal("price", 32, 2)
}

object SaleTable : IntIdTable("sale") {
    val personId = reference("user_id", PersonTable)
    val productId = reference("product_id", ProductTable)
    val amount = integer("amount")
}