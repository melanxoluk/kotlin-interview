package ru.interview.kotlin

import org.jetbrains.exposed.sql.ResultRow
import java.math.BigDecimal


data class Person(
    val id: Int,
    val email: String,
    val name: String
) {
    constructor(row: ResultRow) : this(
        row[PersonTable.id].value,
        row[PersonTable.email],
        row[PersonTable.name],
    )
}

data class Product(
    val id: Int,
    val name: String,
    val price: BigDecimal
) {
    constructor(row: ResultRow) : this(
        row[ProductTable.id].value,
        row[ProductTable.name],
        row[ProductTable.price]
    )
}

data class Sale(
    val id: Int,
    val personId: Int,
    val productId: Int,
    val amount: Int
) {
    constructor(row: ResultRow) : this(
        row[SaleTable.id].value,
        row[SaleTable.personId].value,
        row[SaleTable.productId].value,
        row[SaleTable.amount]
    )
}