package ru.interview.kotlin

import java.math.BigDecimal
import kotlin.test.Test

class ProductsServiceTest {

    @Test
    fun test() {
        assert(
            productsDuplicates.toSet() == setOf(
                Product(1, "ручка", BigDecimal(10)),
                Product(2, "карандаш", BigDecimal(20)),
                Product(5, "ручка", BigDecimal(10)),
                Product(6, "карандаш", BigDecimal(20)),
                Product(8, "пенал", BigDecimal(60)),
                Product(9, "пенал", BigDecimal(60)),
                Product(13, "ручка", BigDecimal(10)),
                Product(14, "календарь", BigDecimal(100)),
                Product(22, "календарь", BigDecimal(100))
            )
        )
    }
}