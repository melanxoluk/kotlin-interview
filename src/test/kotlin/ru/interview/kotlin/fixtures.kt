package ru.interview.kotlin

import ru.interview.kotlin.service.ProductsService
import java.math.BigDecimal


val testPersons = listOf(
    Person(1, "ivan@mail.ru", "Иван"),
    Person(2, "dima@mail.ru", "Дима"),
    Person(3, "viktor@mail.ru", "Виктор"),
    Person(4, "semen@mail.ru", "Семен"),
    Person(5, "alex@mail.ru", "Александр")
)

val testProducts = listOf(
    Product(1, "ручка", BigDecimal(10)),
    Product(2, "карандаш", BigDecimal(20)),
    Product(3, "тетрадь", BigDecimal(30)),
    Product(4, "блокнот", BigDecimal(40)),
    Product(5, "ручка", BigDecimal(10)),
    Product(6, "карандаш", BigDecimal(20)),
    Product(7, "сумка", BigDecimal(50)),
    Product(8, "пенал", BigDecimal(60)),
    Product(9, "пенал", BigDecimal(60)),
    Product(10, "скрепки", BigDecimal(70)),
    Product(11, "картон", BigDecimal(80)),
    Product(12, "цветная бумага", BigDecimal(90)),
    Product(13, "ручка", BigDecimal(10)),
    Product(14, "календарь", BigDecimal(100)),
    Product(15, "стикеры", BigDecimal(110)),
    Product(16, "магнит маленький", BigDecimal(120)),
    Product(17, "магнит большой", BigDecimal(130)),
    Product(18, "ластик", BigDecimal(140)),
    Product(19, "акварель", BigDecimal(150)),
    Product(20, "кисточка", BigDecimal(160)),
    Product(21, "циркуль", BigDecimal(170)),
    Product(22, "календарь", BigDecimal(100))
)

val testSales = listOf(
    Sale(1, 1, 1, 1),
    Sale(2, 2, 2, 2),
    Sale(3, 3, 20, 4),
    Sale(4, 1, 16, 5),
    Sale(5, 4, 17, 2),
    Sale(6, 4, 7, 2),
    Sale(7, 3, 8, 3),
    Sale(8, 2, 4, 4),
    Sale(9, 1, 10, 6),
    Sale(10, 2, 3, 2),
    Sale(11, 1, 5, 3),
    Sale(12, 1, 4, 1),
    Sale(13, 3, 9, 3),
    Sale(14, 3, 12, 1),
    Sale(15, 5, 14, 1),
    Sale(16, 4, 16, 2),
    Sale(17, 5, 18, 4),
    Sale(18, 1, 2, 1)
)


val productsDuplicates: List<Product> get() =
    ProductsService().getDuplicates(testProducts)

val ivanSales: List<Sale> get() = TODO()
