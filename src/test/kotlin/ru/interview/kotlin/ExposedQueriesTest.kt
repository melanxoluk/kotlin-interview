package ru.interview.kotlin

import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction
import ru.interview.kotlin.dao.createPersons
import ru.interview.kotlin.dao.createProducts
import ru.interview.kotlin.dao.createSales
import ru.interview.kotlin.dao.getDuplicates
import ru.interview.kotlin.dao.getMostSalesPerson
import ru.interview.kotlin.dao.getSales
import kotlin.test.Test

class ExposedQueriesTest {

    init {
        Database.connect("jdbc:h2:mem:test", driver = "org.h2.Driver")
        transaction {
            SchemaUtils.createMissingTablesAndColumns(PersonTable, ProductTable, SaleTable)

            createPersons(testPersons)
            createProducts(testProducts)
            createSales(testSales)
        }
    }


    @Test
    fun getSalesTest() {
        assert(ivanSales == getSales("Иван"))
    }

    @Test
    fun getMostSalesPersonTest() {
        assert("Иван" == getMostSalesPerson().name)
    }

    @Test
    fun duplicatesTest() {
        assert(productsDuplicates == getDuplicates())
    }
}