package ru.melanxoluk.leetcode

import kotlin.test.Asserter
import kotlin.test.Test
import kotlin.test.assertContentEquals

class TestSolution {

    @Test
    fun test() {
        val nums1 = listOf(
            intArrayOf(1, 2, 3, 0, 0, 0),
            intArrayOf(1),
            intArrayOf(0)
        )

        val m = listOf(
            3,
            1,
            0
        )

        val nums2 = listOf(
            intArrayOf(2, 5, 6),
            intArrayOf(),
            intArrayOf(1)
        )

        val n = listOf(
            3,
            0,
            1
        )

        val res = listOf(
            intArrayOf(1, 2, 2, 3, 5, 6),
            intArrayOf(1),
            intArrayOf(1)
        )
        
        val sol = Solution88()
        for (i in nums1.indices) {
            sol.merge(nums1[i], m[i], nums2[i], n[i])
            assertContentEquals(res[i], nums1[i])
        }
    }
}
